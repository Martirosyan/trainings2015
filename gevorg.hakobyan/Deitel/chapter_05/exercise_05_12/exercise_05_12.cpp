#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter the number: ";
    std::cin  >> number;

    for (int horizontal = 1; horizontal <= number; ++horizontal){
        for (int vertical = 1; vertical <= horizontal; ++vertical){
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int horizontal = 1; horizontal <= number; ++horizontal){
        for (int vertical = 1; vertical <= number + 1 - horizontal; ++vertical){
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int horizontal = 1; horizontal <= number; ++horizontal){
        for (int space = 1; space < horizontal; ++space){
            std::cout << " ";
        }
        for (int vertical = horizontal; vertical <= number; ++vertical){            
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int horizontal = 1; horizontal <= number; ++horizontal){
        for (int space = number; space > horizontal; --space){
            std::cout << " ";
        }
        for (int vertical = number; vertical >= number + 1 - horizontal; --vertical){
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;    

    for (int horizontal = 1; horizontal <= number; ++horizontal){
        for (int firstVertical = 1; firstVertical <= horizontal; ++firstVertical){
            std::cout << "*";
        }
        for (int firstSpace = 1; firstSpace <= number - horizontal; ++firstSpace){
            std::cout << " ";
        }
        std::cout << " ";
        for (int secondVertical = 1; secondVertical <= number + 1 - horizontal; ++secondVertical){
            std::cout << "*";
        }
        for (int secondSpace = 1; secondSpace < horizontal; ++secondSpace){
            std::cout << " ";
        }
        std::cout << " ";
        for (int thirdSpace = 1; thirdSpace < horizontal; ++thirdSpace){
            std::cout << " ";
        }
        for (int thirdVertical = horizontal; thirdVertical <= number; ++thirdVertical){
            std::cout << "*";
        }
        std::cout << " ";
        for (int fourthSpace = number; fourthSpace > horizontal; --fourthSpace){
            std::cout << " ";
        }
        for (int fourthVertical = number; fourthVertical >= number + 1 - horizontal; --fourthVertical){
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    return 0;
}
