#include <iostream>

int
main()
{
    int x, y, z;

    std::cout << "x = ";
    std::cin  >> x;
    std::cout << "y = ";
    std::cin  >> y;
     
    z = x + y;
    std::cout << "z = " << ++z << std::endl; ///we can't use operation in increment ++(x + y)

    return 0;
}
