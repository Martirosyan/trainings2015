#include <iostream>

int
main()
{   
    int number, counter = 1, largest1 = 0, largest2 = 0;

    while (counter <= 10){
        std::cout << "Enter the number: ";
        std::cin  >> number;

        if (number > largest1){
            largest2 = largest1;
            largest1 = number;
        } else if (number > largest2){
            if (number != largest1){
                largest2 = number;
            }
        }

        counter++;     
    }

    std::cout << "First largest number: " << largest1;
    std::cout << "\nSecond largest number: " << largest2 << std::endl;

    return 0;
}
