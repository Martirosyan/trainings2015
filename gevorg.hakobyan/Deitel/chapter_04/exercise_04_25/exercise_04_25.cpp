#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter the number (the number should be in interval 1-20): ";
    std::cin  >> number;

    if (number > 20){
        std::cout << "Error 1: The number should be in interval 1-20.\nTry again." << std::endl;
        return 1;
    }
    if (number < 1){
        std::cout << "Error 1: The number should be in interval 1-20.\nTry again." << std::endl;
        return 1;
    }

    int i = 1;
    while (i <= number){
        int j = 1;
        while (j <= number){
            if (2 > i){
                std::cout << "*";
            } else if (i == number){
                std::cout << "*";         
            } else if (1 == j){ 
                std::cout << "*";
            } else if (j == number){
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++j;
        }
        std::cout << std::endl;
        ++i;
    }

    return 0;
}
