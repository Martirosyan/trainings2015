#include <iostream>

int
main()
{
    int degree = 1;
	
    while (true){
        degree *= 2;
        std::cout << degree << " ";
    }

    std::cout << std::endl;
	
    return 0;
}
