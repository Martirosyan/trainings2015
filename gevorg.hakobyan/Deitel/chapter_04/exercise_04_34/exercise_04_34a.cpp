#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter the four-digit number: " ;
    std::cin  >> number;

    if (number < 1000){
        std::cout << "Error 1: The number should be four-digit.\nTry again." << std::endl;
        return 1;
    }  
    if (number > 9999){
        std::cout << "Error 1: The number should be four-digit.\nTry again." << std::endl;
        return 1;
    }

    int digit1 = (number / 1000 + 7) % 10;
    int digit2 = (number / 100 + 7) % 10;
    int digit3 = (number / 10 + 7) % 10;
    int digit4 = (number + 7) % 10;

    std::cout << "Code: " << digit3 << digit4 << digit1 << digit2 << std::endl;
 
    return 0;
}   
