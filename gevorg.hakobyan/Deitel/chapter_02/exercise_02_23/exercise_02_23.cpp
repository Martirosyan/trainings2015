#include <iostream>

int 
main()
{ 
    int a, b, c, d, e;
    int min , max;

    std::cout << "Enter 5 integers: ";
	
    std::cin >> a >> b >> c >> d >> e ;

    if (a <= b) 
        min = a;

    if (a > b)
        min = b;

    if (min >= c)
        min = c;

    if (min >= d)
        min = d;

    if (min <= e)
        std::cout << "MIN = " << min << std::endl;

    if (min > e)
        std::cout << "MIN = " << e  << std::endl;
    
    if (a <= b)
        max = b;

    if (a > b)
        max = a;

    if (max <= c)
        max = c;

    if (max <= d)
        max = d;

    if (max <= e)
        std::cout << "MAX = " << e << std::endl;

    if (max > e)
        std::cout << "MAX = " << max  << std::endl;

    return 0;

}
