#include <iostream>

int
main()
{
    int command, register1;
    int a, b, c, d, e, f, g;
start:
    std::cout << "Command Set" << std::endl;
    std::cout << "0 - exit\n";
    std::cout << "1 - load\n";
    std::cout << "2 - add\n";
    std::cout << "3 - sub\n";
    std::cout << "4 - mul\n";
    std::cout << "5 - div\n";
    std::cout << "6 - print\n" << std::endl;
    
    std::cout << "Command: ";
    std::cin  >> command;
    
    /// exit handling
    if (0 == command) {
        std::cout << "Closing the Mini Computer..." << std::endl;
        return 0;
    }
    
    /// load command handling
    if (1 == command) {
        std::cout << "\nLoad (into one of the following registers)" << std::endl;
        std::cout << "0 - a\n";
        std::cout << "1 - b\n";
        std::cout << "2 - c\n";
        std::cout << "3 - d\n";
        std::cout << "4 - e\n";
        std::cout << "5 - f\n";
        std::cout << "6 - g\n" << std::endl;
        
        std::cout << "Register 1: ";
        std::cin  >> register1;
        
        if(register1 < 0) {
            std::cout << "Error 2: Register not found" << std::endl;
            return 2;
        }
        
        if(register1 > 6) {
            std::cout << "Error 2: Register not found" << std::endl;
            return 2;
        }
                
        /// load register a
        if(0 == register1) {
            std::cout << "\nLoad into a\n";
            std::cout << "Input the value to load into register a\n" << std::endl;
            
            std::cout << "a: ";
            std::cin  >> a;
        }
        
        /// load register b
        if(1 == register1) {
            std::cout << "\nLoad into b\n";
            std::cout << "Input the value to load into register b\n" << std::endl;
            
            std::cout << "b: ";
            std::cin  >> b;
        }
        
        /// load register c
        if(2 == register1) {
            std::cout << "\nLoad into c\n";
            std::cout << "Input the value to load into register c\n" << std::endl;
            
            std::cout << "c: ";
            std::cin  >> c;
        }
        
        /// load register d
        if(3 == register1) {
            std::cout << "\nLoad into d\n";
            std::cout << "Input the value to load into register d\n" << std::endl;
            
            std::cout << "d: ";
            std::cin  >> d;
        }
        
        /// load register e
        if(4 == register1) {
            std::cout << "\nLoad into e\n";
            std::cout << "Input the value to load into register e\n" << std::endl;
            
            std::cout << "e: ";
            std::cin  >> e;
        }
        
        /// load register f
        if(5 == register1) {
            std::cout << "\nLoad into f\n";
            std::cout << "Input the value to load into register f\n" << std::endl;
            
            std::cout << "f: ";
            std::cin  >> f;
        }
        
        /// load register g
        if(6 == register1) {
            std::cout << "\nLoad into g\n";
            std::cout << "Input the value to load into register g\n" << std::endl;
            
            std::cout << "g: ";
            std::cin  >> g;
        }
        
        goto start;
    }
    
    /// add command handling
    if (2 == command) {
        goto start;
    }
    
    /// sub command handling
    if (3 == command) {
        goto start;
    }
    
    /// mul command handling
    if (4 == command) {
        goto start;
    }
    
    /// div command handling
    if (5 == command) {
        goto start;
    }
    
    /// print command handling
    if (6 == command) {
        goto start;
    }
    
    std::cout << "Error 1: Command not found" << std::endl;
    return 1;
}

