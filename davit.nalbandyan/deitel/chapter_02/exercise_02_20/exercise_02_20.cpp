#include <iostream>

int
main()
{
    int R;
    std::cout << "Enter the Radius: " << std::endl;
    std::cin >> R;
    std::cout << "Diameter " << 2 * 3.14159 << std::endl;
    std::cout << "Circle " << 2 * 3.14159 * R << std::endl;
    std::cout << "Area " << 3.14159 * R * R << std::endl;

    return 0;
}
