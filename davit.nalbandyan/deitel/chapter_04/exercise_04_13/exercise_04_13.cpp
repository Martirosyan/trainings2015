#include <iostream>

int 
main()
{
    double distance = 0, totalDistance = 0, totalConsumption = 0;

    while(distance != -1)
    {
        std::cout << "\nEnter the distance traveled (-1 if the input is finished): ";
        std::cin  >>  distance;
        if(-1 > distance){
            std::cout << "\nInput not valid, closing program\n";
            return 1;
        }
        totalDistance += distance;
        if(-1 == distance){
            return 0;
        }
        double consumption;
        std::cout << "\nEnter the consumption of gasoline: ";
        std::cin  >> consumption;
        if(consumption < 0){
            std::cout << "\nInput not valid, closing program\n";
            return 3;
        }
        totalConsumption += consumption;
        double average = totalDistance / totalConsumption;
        std::cout << "\nThe total value of the miles / gallon: " << average
                  << std::endl;
    }
    return 0;
}
