/// This program reads out the radius of a circle and prints diameter of a circle,length of a circle and the area 
#include <iostream> /// standard input output
 
/// function main begins program execution
int
main()
{
    int r, d, L, S; /// announcement of variables

    std::cout << "Enter the radius of circle r: " << std::endl; /// dipslay the value of "Radius";end line
    std::cin >> r;  /// reads number from user into "r"

    if (r > 0) { /// enter the radius of circle "r"
        std::cout << "Diameter of circle is d = 2 * r = 2 *  " << r << " = " << 2*r << std::endl; /// display the value of "Diameter";end line
        std::cout << "Length of circle is L = 2* pi * r = 2 * " << 3.14159 << " * " << r << " = " << 2*3.14159*r << std::endl; /// display the value of "Length";end line
        std::cout << "Area of circle id S = pi * r * r = " << 3.14159 << " * " << r << " * "<< r << " = " << 3.14159*r*r << std::endl; /// diplay the value of "Area";end line
    }
    if (r <= 0) /// enter the radius of circle "r"
        std::cout << "You enter wrong number.The radius can`t be eqaul or less than 0 " << std::endl; /// display the notify to user what happened
    return 0; /// program ended successfully
} /// end function main
 
