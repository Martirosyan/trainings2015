#include <iostream>

int
main()
{
    double totalSold = 0;
    for (int day = 1; day <= 7; ++day) {
        std::cout << "Day " << day << std::endl;
        for(int counter = 0; counter < 5; ++counter) {
            int productNumber;
            std::cout << "Enter product number: ";
            std::cin >> productNumber;
          
            int quantitySold; 
            std::cout << "Enter quantity sold: ";
            std::cin >> quantitySold;
            if (quantitySold < 0) {
                std::cerr << "Error 1. Quantity sold should be positive." << std::endl;     
                return 1;
            }
           
            switch (productNumber) {    
            case 1: totalSold += quantitySold * 2.98; break;
            case 2: totalSold += quantitySold * 4.50; break;
            case 3: totalSold += quantitySold * 9.98; break;
            case 4: totalSold += quantitySold * 4.49; break;     
            case 5: totalSold += quantitySold * 6.87; break;
            default:
                std::cerr << "Error 2. Entered product number does not exists. Try again." << std::endl;
                return 2;
            }
        }
    }
    std::cout << std::endl;
    std::cout << "Total retail value of products for week is "<< totalSold << std::endl;

    return 0;
}         
