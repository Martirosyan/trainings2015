a) for(int x = 100; x >= 1; x--) {
       std::cout << x << std::endl;
   }
  
  
b) switch(value % 2) {
   case 0:
       std::cout << "Even integer" << std::endl;
       break;
   case 1:
       std::cout << "Odd integer" << std::endl;
       break;
   }
   
   
c) for(int x = 19; x >= 1; x -= 2) {
       std::cout << x << std::endl;
   }
   
   
d) int counter = 2;
    
    do {
        std::cout << counter << std::endl;
        counter += 2;
    } while(counter <= 100);
