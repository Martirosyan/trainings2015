/// GradeBook class testing
#include <iostream>
#include "GradeBook.hpp" /// Implementation of design’s GradeBook

/// function main begins program execution
int
main()
{
    /// Create objects of GradeBook and order them to “show”
    std::string nameOfTeacher;
    GradeBook gradeBook1( "C++ Programming trainings", "\nArtak is teacher of our course");
    std::cout << "gredeBook created for: " << gradeBook1.getCourseName() << gradeBook1.getTeacherName() << std::endl;
    std::cout << "Please enter the teacher name: ";
    std::getline(std::cin, nameOfTeacher);
    gradeBook1.setTeacherName(nameOfTeacher);
    std::cout << std::endl;
    gradeBook1.displayMessage();
   
    return 0; /// program ended successfully 
} /// end function main

