/// This program defines a salary for each of several employees
#include <iostream>
#include <iomanip>

int
main()
{
    int workTime;
    std::cout << "Enter number of working hours (negative to quit): ";
    std::cin >> workTime;
    
    while(workTime >= 0)
    {
        double hourlyRate;
        std::cout << "Enter an hourly rate of the worker ($00.00): ";
        std::cin >> hourlyRate;
            
        if(hourlyRate < 0) {
            std::cout << "Error1. Entered hourly rate " << hourlyRate << " is wrong." << std::endl;
            return 1;
        }
        if(workTime <= 40) {
            std::cout << "Salary: $" << (workTime * hourlyRate) << std::endl;
        } else {
            std::cout << "Salary: $" << (40 * hourlyRate + (workTime - 40) * (hourlyRate * 1.5)) << std::endl;
        }
            
        std::cout << "Enter number of working hours (negative to quit): ";
        std::cin >> workTime;
    }
    
    return 0;
}
