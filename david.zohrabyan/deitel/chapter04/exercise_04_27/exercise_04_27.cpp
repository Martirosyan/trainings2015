#include <iostream>

int
main()
{
    int binaryWhole;
    std::cout << "Enter the binary whole number: ";
    std::cin >> binaryWhole;
        
    if(binaryWhole < 0 ) {
        std::cerr << "Error1. Binary whole must be positive" << std::endl;
        return 1;
    }
    
    int binaryBase = 1, decimalNumber = 0;
    while(binaryWhole > 0)
    {   
        int binaryDigit = binaryWhole % 10;
        if(binaryDigit > 1) {
            std::cerr << "Error2. This in not a binary whole number" << std::endl;
            return 2;
        }
        
        decimalNumber += binaryDigit * binaryBase;
        binaryBase *= 2;
        binaryWhole /= 10;
    
    }
    
    std::cout << decimalNumber << std::endl;

    return 0;
}
