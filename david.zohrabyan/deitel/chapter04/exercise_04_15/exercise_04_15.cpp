/* This program enters for each seller the volume of its sales for the last  
week to count and display its earnings. Data are entered  
serially for each seller.*/
#include <iostream>
#include <iomanip>

int
main()
{
    double salesVolume;
    std::cout << "Enter sales volume in dollars (negative to quit): ";
    std::cin >> salesVolume;
    
    while(salesVolume >= 0)
    {
        std::cout << "Earnings: $" << std::setprecision(2) << std::fixed <<  (200 + (salesVolume * 9) / 100) << std::endl;
        
        std::cout << "Enter sales volume in dollars (negative to quit): ";
        std::cin >> salesVolume;
    }
    
    return 0;
}
