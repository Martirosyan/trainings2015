#include <iostream>
#include "Encryption.hpp"

int
main()
{
    int actionChoice;
    std::cout << "Enter 1 to encode or 2 to decode: ";
    std::cin >> actionChoice;    
    
    int data;
    std::cout << "Enter a number to encrypt/decrypt: ";
    std::cin >> data;
    
    if(data > 9999) {
        std::cout << "Error 1: The phone number must be four digit." << std::endl;
        return 1;
    } else if(data < 0) {
        std::cout << "Error 1: The phone number must be four digit." << std::endl;
        return 1;
    }
    
    Encryption user;
    if(1 == actionChoice) {
        user.encode(data);
    } else if(2 == actionChoice) {
        user.decode(data);
    } else {
        std::cout << "Error 2: Either encryption or decryption should be selected." << std::endl;
	    return 2;
    }
     
    return 0;
}
