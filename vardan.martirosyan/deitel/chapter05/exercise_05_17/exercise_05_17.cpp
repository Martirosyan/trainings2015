#include <iostream>

int
main()
{
    int i = 1, r, j = 2, k = 3, m = 2;

    std::cout << (i == 1) <<  std::endl;
    std::cout << ( j == 3 ) << std::endl;
    std::cout << ( i >= 1 && j < 4 ) << std::endl;
    std::cout << (m <= 99 && k < m )<< std::endl;
    std::cout << ( j >= i || k == m ) << std::endl;
    std::cout << (k + m<j || 3-j>=k) << std::endl;
    std::cout << ( !m ) << "7 " << std::endl;   /// необходимо
    std::cout <<  ! ( j - m )  << std::endl;   /// необходимо
    std::cout <<  (! ( r > m ) ) << std::endl;   /// необходимо

    return 0;
}


///1
///0
///1
///0
///1
///0
///07 
///1
///1

