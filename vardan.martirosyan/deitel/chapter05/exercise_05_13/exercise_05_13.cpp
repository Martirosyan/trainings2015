#include <iostream>

int
main()
{   
    for (int i, j = 1; j <= 5; j++ ){
        std::cout << "Еnter number from 1 to 30: ";
        std::cin  >> i;
       
        if (i > 30 || i < 1){
            std::cout << "Error: Please re-open the program and write the right number." << std::endl;
            return 0;
        }
        for (int h = 1; h <= i; h++){
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0;
}

