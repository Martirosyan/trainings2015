#include <iostream>

int
main()
{
    int a, k, o = 1 ;

    std::cout << "Enter number: ";
    std::cin  >> a;

    if (a < 0){
        std::cout << "Error your number low in 0; pleace Enter number in >= 0" << std::endl;
        return 1;
    }
    for (int i = 1, b; i <= a; i++){
        std::cout << "Enter number " << o++ << ": ";
        std::cin  >> b;
        k += b;
    }
    std::cout << "Sum is: " << k << std::endl;
    return 0;
}
