#include <iostream>

int
main()
{
    int sum = 0, i = 1;
 
    for (int a; i >= 0; i++){
        std::cout << "Enter number: ";
        std::cin  >> a;
        sum += a;  
        if (a == 9999){
            std::cout << "Average value: " << sum / i << std::endl;
            return 2;
        }
    }
}
