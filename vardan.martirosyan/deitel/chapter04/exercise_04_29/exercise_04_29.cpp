#include <iostream>

int
main()
{
    int a = 0, b = 2;

    while (a >= 0)
    {
        std::cout << b << ", ";
        a = a + 2;
        b = 2 * a; 
    }
    return 0;
}
