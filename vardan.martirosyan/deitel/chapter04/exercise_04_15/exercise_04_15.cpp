#include <iostream>

int 
main()
{
    double chemicals, a, earnings;

    while ( 1 > a ){

        std::cout << "Enter sales in dollars: ";
        std::cin  >> chemicals;
        
        if ( chemicals < 0 ){
            std::cout << "End Program." << std::endl;
            return 1;
        }  
      
        earnings = ( chemicals * 9 / 100 ) + 200;
    
        std::cout << "Earnings: $" << earnings << std::endl;

        a--;
    }
    return 0;
}
