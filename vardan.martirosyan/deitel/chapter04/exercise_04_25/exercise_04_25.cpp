#include <iostream>

int
main()
{
    int a, k = 0, f=1;

    std::cout << "Enter square party: ";
    std::cin  >> a;

    if (a < 1 || a > 20){
        std::cout << "ERROR your price high to 20 or low to 1." << std::endl;
        return 3;
    }
    if (a == 1){
        std::cout << "*" << std::endl;
        return 1;
    }
    while (a > k){
        std::cout << "*";
        ++k;
    }
    std::cout << std::endl;

    while (a > 2){
        std::cout << "*";
        int i = k;
        while (i > 2){      
            std::cout << " ";
            i--;
        }
    std::cout << "*\n";
    a--;  
    }
    
    while (k >= f){
        std::cout << "*";
        f++;
    }
    std::cout << std::endl;
    return 0;
}
