#include <iostream>

int
main ()
{
    int number, digit1, digit2, digit3, digit4, digit5;
    
    std::cout << "Enter five-digit number: ";
    std::cin >> number;
    
    if (number <= 9999 || number >= 100000){
        std::cout << "ERROR your figure not five-digit. Please start the program once again and enter five-digit.\n";
        return 0;
    }

    digit1 = number / 10000;
    digit2 = (number / 1000) % 10;
    digit3 = (number / 100) % 10;
    digit4 = (number / 10) % 10;
    digit5 = number % 10;

    if (digit1 == digit5){
        if (digit2 == digit4)
            std::cout << "Polindrom:" << std::endl;
            return 2;
    }
    else {
        std::cout << "Not Polindrom:" << std::endl;
        return 3;
    }
}
