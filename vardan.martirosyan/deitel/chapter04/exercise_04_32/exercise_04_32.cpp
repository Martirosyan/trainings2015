#include <iostream>

int
main()
{
    double a, b, c;

    std::cout << "Enter triangle side 1: ";
    std::cin  >> a;

    std::cout << "Enter triangle side 2: ";
    std::cin  >> b;

    std::cout << "Enter triangle side 3: ";
    std::cin  >> c;

    if( a <= 0 || b <= 0 || c <= 0 ){
        std::cout << "Error1." << std::endl;
        return 1;
    }

    if ( (a + b) > c && (a + c) > b && (b + c) > a ){
        std::cout << "Thrue" << std::endl;
    }
    else {
        std::cout << "False" << std::endl;
    }
    return 0;


}
