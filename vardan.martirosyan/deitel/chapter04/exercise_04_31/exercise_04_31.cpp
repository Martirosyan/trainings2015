#include <iostream>

int
main()
{
    int x, y , z, c = 0;

    std::cout << "Enter X: " ;
    std::cin  >> x ;
    std::cout << "Enter Y: " ;
    std::cin  >> y ;
    
    while (c != 1){
        z = x + y;
        std::cout << "z = " << ++z << std::endl; ///we can't use operation in increment ++(x + y)
        c++;
    }
    return 0;
}
