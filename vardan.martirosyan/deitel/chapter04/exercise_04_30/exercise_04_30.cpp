#include <iostream>

int
main()
{
    double r, diameter, circumference, area, pi = 3.14159;
    
    std::cout << "Enter R: ";
    std::cin  >> r;

    if ( r <= 0 ){
        std::cout << "Error1. The Radius must be positive" << std::endl;
        return 1;
    }
    std::cout << "Diameter = " << 2 * r << std::endl;

    std::cout << "Circumference = " << pi * 2 * r << std::endl;

    std::cout << "Area = " <<  pi * r * r << std::endl;
    
    return 0;
}
