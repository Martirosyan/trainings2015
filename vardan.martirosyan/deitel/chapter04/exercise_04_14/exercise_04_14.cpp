#include <iostream>

int 
main()
{
    int n;
    double ib, e, p, c, nb;

    std::cout << "Enter number of account (non-positive number to quit): " ;
    std::cin  >> n ;
   
    while (n > 0){
        std::cout << "Enter initial balance: " ;
        std::cin  >> ib;
        std::cout << "Enter the sum of expenses: ";
        std::cin  >> e;
        std::cout << "Enter the sum of parish: ";
        std::cin  >> p;
        std::cout << "Enter the limit of the credit: ";
        std::cin  >> c;

        nb = ib + e - p;
        std::cout << "New balance: " << nb ;
        if ( nb > c ){
            std::cout << "\nAccounts: " << n ;
            std::cout << "\nThe limit of the credit: " << c ;
            std::cout << "\nBalance: " << nb ;
            std::cout << "\nThe limit of the credit exceeded" ;
        }
        std::cout << "\n\nEnter number of account (non-positive number to quit): " ;
        std::cin  >> n ;
    }
    return 0;
}
