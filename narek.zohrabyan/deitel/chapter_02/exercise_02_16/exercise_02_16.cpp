#include <iostream>

int 
main()
{
    int i, j;

    std::cout << "Enter numbers:\n";
    std::cin >> i >> j;
    std::cout << "sum = " << i + j << std::endl;
    std::cout << "product = " << i * j << std::endl;
    std::cout << "difference = " << i - j << std::endl;
    std::cout << "quotient = " << i / j << std::endl;

    return 0;
}
